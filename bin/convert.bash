#!/bin/bash

set -e

echo "$(date --iso-8601=s)" new file "$1"

if [[ "$1" == */.* ]]; then
    echo "$(date --iso-8601=s)" "$1" is a hidden file
    exit 1
fi

if [[ "${1^^}" != *.HEIC ]]; then
    echo "$(date --iso-8601=s)" "$1" is not an HEIC file
    exit 1
fi

shopt -s nocasematch
filename="${1/%.HEIC/.jpg}"
dir=$(mktemp -d)
tmp="$dir/$(basename $filename)"
sleep 2

set -x

heif-convert "$1" "$tmp"
cp "$tmp" .
chown --reference="$1" "$filename"
rm "$1"
rm -rf "$dir"
