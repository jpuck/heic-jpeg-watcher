FROM ubuntu:22.04

WORKDIR /images

COPY ./bin/ /usr/local/bin

RUN apt-get update \
  && apt-get install -y inotify-tools gawk libheif-examples \
  && apt-get autoremove -y && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# https://stackoverflow.com/a/38808856/4233593
CMD inotifywait -m -e close_write /images | gawk '{print $1$3; fflush()}' | xargs -L 1 convert.bash
