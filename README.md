# HEIC -> JPEG Converter Service

This service will monitor a folder for any HEIC files that are created,
and them convert them into a JPEG.

See [example production docker compose file](./prod-example-docker-compose.yml)

Mount your folder to the `/images` volume.
